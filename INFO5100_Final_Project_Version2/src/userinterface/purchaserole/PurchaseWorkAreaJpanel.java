/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.purchaserole;

import business.EcoSystem;
import business.enterprise.Enterprise;
import business.organization.FinaceOrganization;
import business.organization.Organization;
import business.useraccount.UserAccount;
import java.awt.CardLayout;
import javax.swing.JPanel;
import userinterface.developmentrole.ManagerViewRequestJPanel;

/**
 *
 * @author Accelarator
 */
public class PurchaseWorkAreaJpanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private EcoSystem business;
    private UserAccount userAccount;
    private FinaceOrganization finaceOrganization;
    private Enterprise enterprise;

    public PurchaseWorkAreaJpanel(JPanel userProcessContainer, UserAccount account, Organization organization, EcoSystem business, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = account;
        this.finaceOrganization = (FinaceOrganization) organization;
        this.enterprise = enterprise;
        this.business = business;
        
        this.usernameLabel.setText(account.getUsername());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        usernameLabel = new javax.swing.JLabel();
        viewrequestButton = new javax.swing.JButton();
        viewstockButton = new javax.swing.JButton();

        jLabel1.setText("Purchase role work area:       Welcome!");

        usernameLabel.setText("username");

        viewrequestButton.setText("View Request");
        viewrequestButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewrequestButtonActionPerformed(evt);
            }
        });

        viewstockButton.setText("View Inventory");
        viewstockButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewstockButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jLabel1)
                        .addGap(26, 26, 26)
                        .addComponent(usernameLabel))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(227, 227, 227)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(viewstockButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(viewrequestButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(321, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(usernameLabel))
                .addGap(82, 82, 82)
                .addComponent(viewrequestButton)
                .addGap(53, 53, 53)
                .addComponent(viewstockButton)
                .addContainerGap(275, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void viewrequestButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewrequestButtonActionPerformed
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("PurchaseViewRequestJPanel", new PurchaseViewRequestJPanel(userProcessContainer, userAccount, finaceOrganization, business, enterprise));
        layout.next(userProcessContainer);      // TODO add your handling code here:        // TODO add your handling code here:
    }//GEN-LAST:event_viewrequestButtonActionPerformed

    private void viewstockButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewstockButtonActionPerformed
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("PurchaseViewInventoryJPanel", new PurchaseViewInventoryJPanel(userProcessContainer, userAccount, finaceOrganization, business, enterprise));
        layout.next(userProcessContainer);      // TODO add your handling code here:  // TODO add your handling code here:
    }//GEN-LAST:event_viewstockButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel usernameLabel;
    private javax.swing.JButton viewrequestButton;
    private javax.swing.JButton viewstockButton;
    // End of variables declaration//GEN-END:variables
}
