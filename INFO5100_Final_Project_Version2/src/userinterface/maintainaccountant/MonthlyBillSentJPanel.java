/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.maintainaccountant;

import business.EcoSystem;
import static business.RandomGenerator.randInt;
import business.bill.MonthlyBill;
import business.customer.CustomerWaterUsage;
import business.enterprise.Enterprise;
import business.organization.CustomerOrganization;
import business.organization.FinaceOrganization;
import business.organization.Organization;
import business.useraccount.UserAccount;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public class MonthlyBillSentJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private EcoSystem business;
    private UserAccount userInfoJTabel;
    private FinaceOrganization finaceOrganization;
    private Enterprise enterprise;
    
    public MonthlyBillSentJPanel(JPanel userProcessContainer, Organization organization, EcoSystem business, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.finaceOrganization = (FinaceOrganization) organization;
        this.enterprise = enterprise;
        this.business = business;
        populateTable();
    }

    public void populateTable(){
        DefaultTableModel model = (DefaultTableModel) this.accountInfoTable.getModel();
        model.setRowCount(0);
        CustomerOrganization co=null;
        for(Organization o:enterprise.getOrganizationDirectory().getOrganizationList()){
            if(o instanceof CustomerOrganization){
                co=(CustomerOrganization)o;
                break;
            }
        }
        for (UserAccount ua : co.getUserAccountDirectory().getUserAccountList()) {
            try {
                Object[] row = new Object[4];
                row[0] = ua;
                row[2] = ua.getCustomer().getWaterUsageList().get(ua.getCustomer().getWaterUsageList().size() - 1).getWaterAmount();
                row[3] = ua.getCustomer().getWaterUsageList().get(ua.getCustomer().getWaterUsageList().size() - 1).getStartDate();
                row[1] = (double) 49.0 / 1000.0 * (ua.getCustomer().getWaterUsageList().get(ua.getCustomer().getWaterUsageList().size() - 1).getWaterAmount())
                        + 67.0 / 1000 * (ua.getCustomer().getWaterUsageList().get(ua.getCustomer().getWaterUsageList().size() - 1).getWaterAmount());
                model.addRow(row);
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }

    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        accountInfoTable = new javax.swing.JTable();
        sentBillBtn = new javax.swing.JButton();

        jLabel1.setText("User current monthly bill information:");

        accountInfoTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "User name", "Bill charge", "Water usage", "Start date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(accountInfoTable);
        if (accountInfoTable.getColumnModel().getColumnCount() > 0) {
            accountInfoTable.getColumnModel().getColumn(0).setResizable(false);
            accountInfoTable.getColumnModel().getColumn(1).setResizable(false);
            accountInfoTable.getColumnModel().getColumn(2).setResizable(false);
            accountInfoTable.getColumnModel().getColumn(3).setResizable(false);
        }

        sentBillBtn.setText("Send Bill for all");
        sentBillBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sentBillBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(sentBillBtn)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(23, 23, 23)
                            .addComponent(jLabel1))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(49, 49, 49)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 504, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(57, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(sentBillBtn)
                .addContainerGap(219, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void sentBillBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sentBillBtnActionPerformed

        for (int i = 0; i < this.accountInfoTable.getRowCount(); i++) {
            UserAccount ua = (UserAccount) this.accountInfoTable.getValueAt(i, 0);
            MonthlyBill mb = new MonthlyBill();
            mb.setReceiver(ua);
            mb.setCustomerWaterUsage(ua.getCustomer().getWaterUsageList().get(ua.getCustomer().getWaterUsageList().size() - 1));
            mb.setWaterCost((int) (49.0 / 1000.0 * (ua.getCustomer().getWaterUsageList().get(ua.getCustomer().getWaterUsageList().size() - 1).getWaterAmount())));
            mb.setSewerCost((int) (67.0 / 1000.0 * (ua.getCustomer().getWaterUsageList().get(ua.getCustomer().getWaterUsageList().size() - 1).getWaterAmount())));
            ua.getCustomer().getBillHistory().addBill(mb);
            
            CustomerWaterUsage cwu=new CustomerWaterUsage();
            cwu.setWaterAmount(randInt(500,2000));
            cwu.setSewerAmont(randInt(500,2000));
            ua.getCustomer().getWaterUsageList().add(cwu);
        }
        
        populateTable();
        
    }//GEN-LAST:event_sentBillBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable accountInfoTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton sentBillBtn;
    // End of variables declaration//GEN-END:variables
}
