/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.constructor;

import business.EcoSystem;
import business.enterprise.Enterprise;
import business.organization.ResourceManagerOrganization;
import business.useraccount.UserAccount;
import business.workqueue.CallForDevelopmentWorkRequest;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Accelarator
 */
public class ConstructorViewDetailJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private EcoSystem business;
    private Enterprise enterprise;
    private ResourceManagerOrganization resourceManagerOrganization;
    private UserAccount userAccount;
    private CallForDevelopmentWorkRequest request = new CallForDevelopmentWorkRequest();

    public ConstructorViewDetailJPanel(JPanel userProcessContainer, EcoSystem business, Enterprise enterprise, UserAccount account, CallForDevelopmentWorkRequest request) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        this.enterprise = enterprise;
        this.userAccount = account;
        this.request = request;

        nameTextField.setText(request.getResource().getWaterResourceName());
        locationTextField.setText(request.getResource().getLocation());
        descriptionTextField.setText(request.getResource().getDescription());
        boolean a = request.getResource().isDrinkingOrNot();
        if (a) {
            yesdrinkRadioButton.setSelected(a);
        } else {
            nodrinkRadioButton.setSelected(true);
        }

        boolean b = request.getResource().isDryseasonOrNot();
        if (b) {
            yesdryRadioButton.setSelected(b);
        } else {
            nodryRadioButton.setSelected(true);
        }
        quanlityTextField.setText(String.valueOf(request.getResource().getWaterQuantity()));
        storageTextField.setText(String.valueOf(request.getResource().getStorageCapacity()));
        flowTextField.setText(String.valueOf(request.getResource().getFlowVolume()));
        irrigationTextField.setText(String.valueOf(request.getResource().getIrrigationArea()));
        annualTextField.setText(String.valueOf(request.getResource().getAnnualFlow()));
    }



    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        yesdryRadioButton = new javax.swing.JRadioButton();
        nodrinkRadioButton = new javax.swing.JRadioButton();
        nodryRadioButton = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        yesdrinkRadioButton = new javax.swing.JRadioButton();
        jLabel11 = new javax.swing.JLabel();
        nameTextField = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        quanlityTextField = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        locationTextField = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        storageTextField = new javax.swing.JTextField();
        descriptionTextField = new javax.swing.JTextField();
        flowTextField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        irrigationTextField = new javax.swing.JTextField();
        annualTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        backButton = new javax.swing.JButton();

        yesdryRadioButton.setText("Yes");
        yesdryRadioButton.setEnabled(false);

        nodrinkRadioButton.setText("No");
        nodrinkRadioButton.setEnabled(false);

        nodryRadioButton.setText("No");
        nodryRadioButton.setEnabled(false);

        jLabel2.setText("Resource Name");

        jLabel10.setText("Able to drink");

        yesdrinkRadioButton.setText("Yes");
        yesdrinkRadioButton.setEnabled(false);

        jLabel11.setText("Water Quality");

        nameTextField.setEnabled(false);
        nameTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameTextFieldActionPerformed(evt);
            }
        });

        jLabel12.setText("Irrigation Area");

        jLabel3.setText("Location");

        quanlityTextField.setEnabled(false);

        jLabel13.setText("Flow Volume");

        locationTextField.setEnabled(false);

        jLabel14.setBackground(new java.awt.Color(51, 51, 51));
        jLabel14.setText("Annual Flow");

        jLabel4.setText("Description");

        storageTextField.setEnabled(false);

        descriptionTextField.setEnabled(false);

        flowTextField.setEnabled(false);

        jLabel5.setText("Storage Capacity");

        irrigationTextField.setEnabled(false);

        annualTextField.setEnabled(false);

        jLabel6.setText("Dry Season Exist");

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(167, 167, 167)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(locationTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addGap(57, 57, 57)
                                    .addComponent(nameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(84, 84, 84)
                                .addComponent(descriptionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel13)
                                    .addComponent(jLabel14))
                                .addGap(41, 41, 41)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(yesdrinkRadioButton)
                                    .addComponent(yesdryRadioButton)
                                    .addComponent(storageTextField)
                                    .addComponent(flowTextField)
                                    .addComponent(irrigationTextField)
                                    .addComponent(annualTextField)
                                    .addComponent(quanlityTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(102, 102, 102)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nodryRadioButton)
                                    .addComponent(nodrinkRadioButton)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(265, 265, 265)
                        .addComponent(backButton)))
                .addContainerGap(286, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(locationTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(descriptionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(yesdryRadioButton)
                    .addComponent(nodryRadioButton)
                    .addComponent(jLabel6))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(yesdrinkRadioButton)
                    .addComponent(nodrinkRadioButton)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel11)
                    .addComponent(quanlityTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(storageTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(flowTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(97, 97, 97)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(irrigationTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(annualTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14))))
                .addGap(18, 18, 18)
                .addComponent(backButton)
                .addContainerGap(122, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
       userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer); // TODO add your handling code here:        // TODO add your handling code here:
    }//GEN-LAST:event_backButtonActionPerformed

    private void nameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameTextFieldActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField annualTextField;
    private javax.swing.JButton backButton;
    private javax.swing.JTextField descriptionTextField;
    private javax.swing.JTextField flowTextField;
    private javax.swing.JTextField irrigationTextField;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JTextField locationTextField;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JRadioButton nodrinkRadioButton;
    private javax.swing.JRadioButton nodryRadioButton;
    private javax.swing.JTextField quanlityTextField;
    private javax.swing.JTextField storageTextField;
    private javax.swing.JRadioButton yesdrinkRadioButton;
    private javax.swing.JRadioButton yesdryRadioButton;
    // End of variables declaration//GEN-END:variables
}
