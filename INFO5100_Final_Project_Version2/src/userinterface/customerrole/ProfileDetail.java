/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.customerrole;

import business.customer.CustomerWaterUsage;
import business.useraccount.UserAccount;
import java.awt.Color;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author xumengfan
 */
public class ProfileDetail extends javax.swing.JPanel {

    UserAccount useraccount;
    
    public ProfileDetail(UserAccount useraccount) {
        initComponents();
        this.useraccount=useraccount;
        this.populateInfo();
    }

    public void populateInfo(){
        this.TextCustomerAdd.setText(useraccount.getCustomer().getAddress());
        this.TextCustomerName.setText(useraccount.getUsername());
        this.TextCustomerPassword.setText(useraccount.getPassword());
        this.TextCustomerPhone.setText(useraccount.getCustomer().getPhoneNumber());
        
        if(useraccount.getCustomer().isGroupOrNot()){
            jRadioButton2.setSelected(true);
            jRadioButton1.setSelected(false);
        }else{
            jRadioButton2.setSelected(false);
            jRadioButton1.setSelected(true);
        }
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        TextCustomerPhone = new javax.swing.JTextField();
        TextCustomerAdd = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        TextCustomerName = new javax.swing.JTextField();
        TextCustomerPassword = new javax.swing.JTextField();
        modifyBtn = new javax.swing.JButton();
        confirmBtn = new javax.swing.JButton();
        reportCustomerBtn = new javax.swing.JButton();

        jLabel1.setText("User Account Detail:");

        TextCustomerPhone.setEditable(false);

        TextCustomerAdd.setEditable(false);
        TextCustomerAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TextCustomerAddActionPerformed(evt);
            }
        });

        jLabel6.setText("Customer Type:");

        jLabel2.setText("Customer Name:");

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setText("Individual");
        jRadioButton1.setEnabled(false);

        jLabel3.setText("Customer Password:");

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("Group");
        jRadioButton2.setEnabled(false);

        jLabel4.setText("Customer Address:");

        jLabel5.setText("Customer Phone:");

        TextCustomerName.setEditable(false);

        TextCustomerPassword.setEditable(false);

        modifyBtn.setText("Modify");
        modifyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifyBtnActionPerformed(evt);
            }
        });

        confirmBtn.setText("Confirm changes");
        confirmBtn.setEnabled(false);
        confirmBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmBtnActionPerformed(evt);
            }
        });

        reportCustomerBtn.setText("Water Usage Report");
        reportCustomerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportCustomerBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TextCustomerAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(TextCustomerPassword, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                                .addComponent(TextCustomerName, javax.swing.GroupLayout.Alignment.LEADING))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5))
                        .addGap(46, 46, 46)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jRadioButton1)
                                .addGap(29, 29, 29)
                                .addComponent(jRadioButton2))
                            .addComponent(TextCustomerPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(modifyBtn)
                                .addGap(56, 56, 56)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(reportCustomerBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(confirmBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap(117, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(TextCustomerName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TextCustomerPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TextCustomerAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(TextCustomerPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2))
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(modifyBtn)
                    .addComponent(confirmBtn))
                .addGap(18, 18, 18)
                .addComponent(reportCustomerBtn)
                .addContainerGap(34, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void TextCustomerAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TextCustomerAddActionPerformed
        
    }//GEN-LAST:event_TextCustomerAddActionPerformed

    private void modifyBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifyBtnActionPerformed

        this.TextCustomerAdd.setEnabled(true);
        this.TextCustomerName.setEnabled(true);
        this.TextCustomerPassword.setEnabled(true);
        this.TextCustomerPhone.setEnabled(true);
        
        this.TextCustomerAdd.setEditable(true);
        this.TextCustomerName.setEditable(true);
        this.TextCustomerPassword.setEditable(true);
        this.TextCustomerPhone.setEditable(true);
        confirmBtn.setEnabled(true);
        
    }//GEN-LAST:event_modifyBtnActionPerformed

    private void confirmBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmBtnActionPerformed
        useraccount.setPassword(TextCustomerPassword.getText());
        useraccount.setUsername(TextCustomerName.getText());
        useraccount.getCustomer().setName(TextCustomerName.getText());
        useraccount.getCustomer().setAddress(TextCustomerAdd.getText());
        useraccount.getCustomer().setPhoneNumber(TextCustomerPhone.getText());
        populateInfo();
        
        this.TextCustomerAdd.setEnabled(false);
        this.TextCustomerName.setEnabled(false);
        this.TextCustomerPassword.setEnabled(false);
        this.TextCustomerPhone.setEnabled(false);
        
        this.TextCustomerAdd.setEditable(false);
        this.TextCustomerName.setEditable(false);
        this.TextCustomerPassword.setEditable(false);
        this.TextCustomerPhone.setEditable(false);
        
        this.confirmBtn.setEnabled(false);

    }//GEN-LAST:event_confirmBtnActionPerformed

    private void reportCustomerBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportCustomerBtnActionPerformed

        DefaultCategoryDataset Dataset = new DefaultCategoryDataset();
        int num = 0;
        for (CustomerWaterUsage cwu : this.useraccount.getCustomer().getWaterUsageList()) {
            num++;
            Dataset.setValue(cwu.getWaterAmount(), "Water Amount", String.valueOf(num) + "th month");
        }

        JFreeChart chart = ChartFactory.createLineChart("Water Usage Report", "Month Count", "Water Amount", Dataset);
        // p.setForegroundAlpha(TOP_ALIGNMENT);
        chart.setBackgroundPaint(Color.GRAY);
        chart.getTitle().setPaint(Color.red);
        CategoryPlot p = chart.getCategoryPlot();

        p.setRangeGridlinePaint(Color.BLUE);
        ChartFrame frame = new ChartFrame("Line Chart", chart);
        frame.setVisible(true);
        frame.setSize(800, 800);
    }//GEN-LAST:event_reportCustomerBtnActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField TextCustomerAdd;
    private javax.swing.JTextField TextCustomerName;
    private javax.swing.JTextField TextCustomerPassword;
    private javax.swing.JTextField TextCustomerPhone;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton confirmBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JButton modifyBtn;
    private javax.swing.JButton reportCustomerBtn;
    // End of variables declaration//GEN-END:variables
}
