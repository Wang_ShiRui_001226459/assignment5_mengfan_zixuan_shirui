/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.developmentrole;

import business.EcoSystem;
import business.enterprise.Enterprise;
import business.enterprise.WaterConservationEnterprise;
import business.organization.FinaceOrganization;
import business.organization.Organization;
import business.organization.ResourceManagerOrganization;
import business.useraccount.UserAccount;
import business.workqueue.CallForDevelopmentWorkRequest;
import business.workqueue.WorkRequest;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Accelarator
 */
public class ManagerViewRequestJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private EcoSystem business;
    private Enterprise enterprise;
    // private ResourceManagerOrganization resourceManagerOrganization;
    private UserAccount userAccount;
    // private CallForDevelopmentWorkRequest request;

    public ManagerViewRequestJPanel(JPanel userProcessContainer, EcoSystem business, Enterprise enterprise, UserAccount account) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        this.enterprise = enterprise;
        // this.resourceManagerOrganization = (ResourceManagerOrganization) organization;
        this.userAccount = account;
        //    this.request = request;
        populateTable();
    }

    public void populateTable() {
        DefaultTableModel model = (DefaultTableModel) Table.getModel();
        model.setRowCount(0);
        for (WorkRequest request : enterprise.getWorkQueue().getWorkRequestList()) {//enterprise.getwork
            CallForDevelopmentWorkRequest req = (CallForDevelopmentWorkRequest) request;
            Object[] row = new Object[7];
            row[0] = req;
            row[1] = req.getResource().getLocation();
            row[2] = req.getResource().getDescription();
            row[3] = req.getResource().getStorageCapacity();
            row[4] = req.getResource().getAdvicedDevelopmentLevel();
            row[5] = req.getResource().getEstimateCost();
            row[6] = request.getStatus();
            model.addRow(row);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        Table = new javax.swing.JTable();
        acceptButton = new javax.swing.JButton();
        denyButton = new javax.swing.JButton();
        viewBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        backButton = new javax.swing.JButton();

        Table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Resource Name", "Location", "Description", "StorageCapacity", "AdvicedLevel", "EstimateCost", "Status"
            }
        ));
        jScrollPane1.setViewportView(Table);

        acceptButton.setText("Accept");
        acceptButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                acceptButtonActionPerformed(evt);
            }
        });

        denyButton.setText("Deny");
        denyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                denyButtonActionPerformed(evt);
            }
        });

        viewBtn.setText("View Detail");
        viewBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewBtnActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel1.setText("We found some new resource!   Request from development!");

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(viewBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(denyButton, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(backButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(acceptButton, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(38, 38, 38)
                            .addComponent(jLabel1))
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 823, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(acceptButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(backButton)))
                .addGap(18, 18, 18)
                .addComponent(denyButton)
                .addGap(18, 18, 18)
                .addComponent(viewBtn)
                .addContainerGap(20, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void acceptButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_acceptButtonActionPerformed
        int selectedRow = Table.getSelectedRow();
        if (selectedRow < 0) {
            return;
        }
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if (organization instanceof FinaceOrganization)//   heriatage  son father cannit be reversed
            {
                CallForDevelopmentWorkRequest request = (CallForDevelopmentWorkRequest) Table.getValueAt(selectedRow, 0);
                organization.getWorkQueue().getWorkRequestList().add(request);
                WaterConservationEnterprise wce=(WaterConservationEnterprise)this.enterprise;
                wce.getWaterResourceDirectory().getWaterResourceDirectory().add(request.getResource());
                request.setStatus("Accept!");
                populateTable();

            }
        }
    }//GEN-LAST:event_acceptButtonActionPerformed

    private void denyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_denyButtonActionPerformed
        int selectedRow = Table.getSelectedRow();
        if (selectedRow < 0) {
            return;
        }
        CallForDevelopmentWorkRequest request = (CallForDevelopmentWorkRequest) Table.getValueAt(selectedRow, 0);
        enterprise.getWorkQueue().getWorkRequestList().remove(request);
        request.setStatus("Deny!");

        populateTable();   // TODO add your handling code here:
    }//GEN-LAST:event_denyButtonActionPerformed

    private void viewBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewBtnActionPerformed
        int selectedRow = Table.getSelectedRow();
        if (selectedRow < 0) {
            return;
        }
        CallForDevelopmentWorkRequest request = (CallForDevelopmentWorkRequest) Table.getValueAt(selectedRow, 0);

        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("RequestDetailJPanel", new RequestDetailJPanel(userProcessContainer, business, enterprise, userAccount, request));
        layout.next(userProcessContainer);        // TODO add your handling code here:        // TODO add your handling code here:
    }//GEN-LAST:event_viewBtnActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Table;
    private javax.swing.JButton acceptButton;
    private javax.swing.JButton backButton;
    private javax.swing.JButton denyButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton viewBtn;
    // End of variables declaration//GEN-END:variables
}
