/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.developmentrole;

import business.EcoSystem;
import business.enterprise.Enterprise;
import static business.enterprise.Enterprise.EnterpriseType.WaterConservationEnterprise;
import business.enterprise.WaterConservationEnterprise;
import business.organization.Organization;
import business.organization.ResourceManagerOrganization;
import business.resource.WaterResource;
import business.useraccount.UserAccount;
import business.workqueue.CallForDevelopmentWorkRequest;
import business.workqueue.WorkRequest;
import java.awt.CardLayout;

import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Accelarator
 */
public class ManagerViewResourceJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private EcoSystem business;
    private WaterConservationEnterprise enterprise;
    private ResourceManagerOrganization resourceManagerOrganization;
    private UserAccount userAccount;

    public ManagerViewResourceJPanel(JPanel userProcessContainer, EcoSystem business, Enterprise enterprise, Organization organization, UserAccount account) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        this.enterprise = (WaterConservationEnterprise)enterprise;
        this.resourceManagerOrganization = (ResourceManagerOrganization) organization;
        this.userAccount = account;
        
        
        populateTable();
    }

    public void populateTable() {
        DefaultTableModel model = (DefaultTableModel) Table.getModel();
        model.setRowCount(0);
        
        //WaterConservationEnterprise waterConservationEnterprise = (WaterConservationEnterprise) this.enterprise; //waterprise heireitage enterprise----mandutory transfer into water company 
        for (WaterResource wr : this.enterprise.getWaterResourceDirectory().getWaterResourceDirectory()/*.getWaterResource()*/) {
            Object[] row = new Object[8];
            row[0] = wr;
            row[1] = wr.getLocation();
            row[2] = wr.getDescription();
            row[3] = wr.getStorageCapacity();
            row[4] = wr.getAdvicedDevelopmentLevel();
            row[5] = wr.getEstimateCost();
            boolean a = wr.isDrinkingOrNot();
            if (a) {
                row[6] = "Drinkable";
            } else {
                row[6] = "UnDrinkable";
                boolean b = wr.isDryseasonOrNot();
                if (b) {
                    row[7] = "Exist DrySeason";
                } else {
                    row[7] = "No DrySeason";

                }
            }
            model.addRow(row);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        Table = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        viewdetailButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();

        Table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Resource Name", "Location", "Description", "StorageCapacity", "AdvicedLevel", "EstimateCost", "Drinkable", "DrySeanson"
            }
        ));
        jScrollPane1.setViewportView(Table);

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel1.setText("Water Resource List       ");

        viewdetailButton.setText("View Detail");
        viewdetailButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewdetailButtonActionPerformed(evt);
            }
        });

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(backButton)
                                .addGap(646, 646, 646)
                                .addComponent(viewdetailButton))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 933, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(backButton)
                    .addComponent(viewdetailButton))
                .addContainerGap(110, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void viewdetailButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewdetailButtonActionPerformed
        int selectedRow = Table.getSelectedRow();
        if (selectedRow < 0) {
            return;
        }
        WaterResource wr = (WaterResource) Table.getValueAt(selectedRow, 0);

        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("WaterResourceDetailJPanel", new WaterResourceDetailJPanel(userProcessContainer, business, enterprise, resourceManagerOrganization, userAccount, wr));
        layout.next(userProcessContainer);        // TODO add your handling code here:
    }//GEN-LAST:event_viewdetailButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);        // TODO a        // TODO add your handling code here:
    }//GEN-LAST:event_backButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Table;
    private javax.swing.JButton backButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton viewdetailButton;
    // End of variables declaration//GEN-END:variables
}
