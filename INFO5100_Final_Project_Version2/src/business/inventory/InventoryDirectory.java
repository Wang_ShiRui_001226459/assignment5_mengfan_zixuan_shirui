/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.inventory;

import business.item.Item;
import java.util.ArrayList;

/**
 *
 * @author Accelarator
 */
public class InventoryDirectory {

    private ArrayList<Inventory> inventoryDirectory;

    public InventoryDirectory() {
        inventoryDirectory = new ArrayList<Inventory>();
        newInventory();
    }

    public void newInventory() {
        Inventory inventory1 = new Inventory();
        inventory1.setProductID(0001);
        inventory1.setProductName("4128-A Pipe MarkeSprinkler");
        inventory1.setStock(60);
        inventory1.setUnitprice(12);

        Inventory inventory2 = new Inventory();
        inventory2.setProductID(0002);
        inventory2.setProductName("Valve 2 Way Brass Hose Connector Wate Pipe");
        inventory2.setStock(16);
        inventory2.setUnitprice(86);

        Inventory inventory3 = new Inventory();
        inventory3.setProductID(0001);
        inventory3.setProductName("Nails");
        inventory3.setStock(300);
        inventory3.setUnitprice(2);

        Inventory inventory4 = new Inventory();
        inventory4.setProductID(0004);
        inventory4.setProductName("P3 Save A Drop Water Meter");
        inventory4.setStock(100);
        inventory4.setUnitprice(14);

        Inventory inventory5 = new Inventory();
        inventory5.setProductID(0005);
        inventory5.setProductName(" Outdoor Sprinkler Timer Irrigation Controller");
        inventory5.setStock(210);
        inventory5.setUnitprice(73);

        Inventory inventory6 = new Inventory();
        inventory6.setProductID(0006);
        inventory6.setProductName("Sharkbite Style push fit 1/2 inch Ball valve");
        inventory6.setStock(7);
        inventory6.setUnitprice(12);

        Inventory inventory7 = new Inventory();
        inventory7.setProductID(0007);
        inventory7.setProductName("AquaPlumber Water Pressure Hose professional Drain Cleaning Tool");
        inventory7.setStock(20);
        inventory7.setUnitprice(9);

        Inventory inventory8 = new Inventory();
        inventory8.setProductID(1008);
        inventory8.setProductName("4-Station Easy-Set Logic Indoor/Outdoor Sprinkler Timer");
        inventory8.setStock(68);
        inventory8.setUnitprice(49);

        Inventory inventory9 = new Inventory();
        inventory9.setProductID(1009);
        inventory9.setProductName("100113P Plumbing Replacement Part Polished Brass");
        inventory9.setStock(30);
        inventory9.setUnitprice(78);

        Inventory inventory10 = new Inventory();
        inventory10.setProductID(1009);
        inventory10.setProductName("H2749.167 EXT Kit for 4/3 & 3/2 Port DIVS-DIA");
        inventory10.setStock(300);
        inventory10.setUnitprice(25);

        Inventory inventory11 = new Inventory();
        inventory11.setProductID(2011);
        inventory11.setProductName("Plumbing Kitchen Drain Versatile Vessel Trap");
        inventory11.setStock(50);
        inventory11.setUnitprice(25);

        Inventory inventory12 = new Inventory();
        inventory12.setProductID(2012);
        inventory12.setProductName("American Standard Shower Arm Bracket");
        inventory12.setStock(5);
        inventory12.setUnitprice(13);

        Inventory inventory13 = new Inventory();
        inventory13.setProductID(1013);
        inventory13.setProductName("1/8\" NPT x 1/4\" Hose ID Black HDPE Adapter");
        inventory13.setStock(300);
        inventory13.setUnitprice(2);

        Inventory inventory14 = new Inventory();
        inventory14.setProductID(1014);
        inventory14.setProductName("5P608 Black Pipe Nipple, Threaded, 1/8x2-1/2 In");
        inventory14.setStock(300);
        inventory14.setUnitprice(13);

        Inventory inventory15 = new Inventory();
        inventory15.setProductID(1015);
        inventory15.setProductName("BANJO HB038 Adapter,3/8 x 3/8 In,Polypropylene");
        inventory15.setStock(92);
        inventory15.setUnitprice(23);

        Inventory inventory16 = new Inventory();
        inventory16.setProductID(1016);
        inventory16.setProductName("LEAD FREE WELL PUMP WATER PRESSURE GAUGE");
        inventory16.setStock(300);
        inventory16.setUnitprice(3);

        this.inventoryDirectory.add(inventory1);
        this.inventoryDirectory.add(inventory2);
        this.inventoryDirectory.add(inventory3);
        this.inventoryDirectory.add(inventory4);
        this.inventoryDirectory.add(inventory5);
        this.inventoryDirectory.add(inventory6);
        this.inventoryDirectory.add(inventory7);
        this.inventoryDirectory.add(inventory8);
        this.inventoryDirectory.add(inventory9);
        this.inventoryDirectory.add(inventory10);
        this.inventoryDirectory.add(inventory11);
        this.inventoryDirectory.add(inventory12);
        this.inventoryDirectory.add(inventory13);
        this.inventoryDirectory.add(inventory14);
        this.inventoryDirectory.add(inventory15);
        this.inventoryDirectory.add(inventory16);
    }

    public ArrayList<Inventory> getInventoryDirectory() {
        return inventoryDirectory;
    }

    public void setInventoryDirectory(ArrayList<Inventory> inventoryDirectory) {
        this.inventoryDirectory = inventoryDirectory;
    }

    public Inventory addInventory() {
        Inventory inventory = new Inventory();
        inventoryDirectory.add(inventory);
        return inventory;
    }

    public void deleInventory(Inventory inventory) {
        inventoryDirectory.remove(inventory);
    }

}
