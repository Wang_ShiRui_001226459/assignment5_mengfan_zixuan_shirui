/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 * 
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public class RandomGenerator {

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }
    
    public static String randIndiName() {
        Random rand = new Random();

        String[] array = {"Amy", "Arthur", "Carrie", "Debbie", "Emma", "Kim", "Youko", "Ling", "Hang", "Nan", "Linden ", "Nancy", "John", "Mitsouko", "Randy", "Ken", "Luke", "Kiko", "Peter", "Natalia", "Fiona", "Alex", "Ryan", "Bob", "Teresa", "Ryan", "Toothless", "Midnight", "Frogface", "Tim", "Peggy", "Charlie", "Allen", "Micheal", "Cameron", "Don", "Anita", "Jude", "Billy", "Sam", "Mickey", "Ian"};
        String name = array[rand.nextInt(array.length)];
        return name;
    }

    public static String randCommunityName() {
        Random rand = new Random();

        String[] array = {"MA", "NY", "AL", "AK", "DE", "GA", "HI", "IL", "LA", "SS", "IA", "IN", "PA", "VA"};
        Collections.shuffle(Arrays.asList(array));
        String CommunityName = array[rand.nextInt(array.length)];
        return CommunityName;
    }

    
}
