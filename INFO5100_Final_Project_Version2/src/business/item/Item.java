/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.item;

/**
 *
 * @author Accelarator
 */
public class Item {

    private int price;
    private int modelNumber;
    private int quantity;
    private String productName;
    private int count = 1;
    private String prodLocation;
    private String prodDescription;
   

    public Item() {

        modelNumber = count;
        count++;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(int modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProdLocation() {
        return prodLocation;
    }

    public void setProdLocation(String prodLocation) {
        this.prodLocation = prodLocation;
    }

    public String getProdDescription() {
        return prodDescription;
    }

    public void setProdDescription(String prodDescription) {
        this.prodDescription = prodDescription;
    }

    @Override
    public String toString() {
        return productName;
    }
}
