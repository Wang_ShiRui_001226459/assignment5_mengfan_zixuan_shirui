/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business.customer;

import java.util.ArrayList;

/**
 * 
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public class CustomerDirectory {

    private ArrayList<Customer> customerList;

    public CustomerDirectory() {
        this.customerList = new ArrayList<>();
    }

    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(ArrayList<Customer> customerList) {
        this.customerList = customerList;
    }

    public Customer createCustomer(String name) {
        Customer c = new Customer();
        c.setName(name);
        customerList.add(c);
        return c;
    }

}
