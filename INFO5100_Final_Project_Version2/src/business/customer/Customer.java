/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business.customer;

import static business.RandomGenerator.randInt;
import business.bill.BillDirectory;
import java.util.ArrayList;

/**
 * 
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public class Customer {

    private String name;
    private int id;
    private static int count = 1;
    private BillDirectory billHistory = new BillDirectory();
    private String phoneNumber;
    private String address;
    private boolean groupOrNot;

    private ArrayList<CustomerWaterUsage> waterUsageList = new ArrayList<>();
    
    String workCompany;
    String memo;
    String ssnnumber;
    
    public Customer() {
        id = count;
        count++;
        for (int i = 0; i < 13; i++) {
            CustomerWaterUsage customerWaterUsage = new CustomerWaterUsage();
            customerWaterUsage.setWaterAmount(randInt(500, 2000));
            customerWaterUsage.setSewerAmont(randInt(500, 2000));
            this.waterUsageList.add(customerWaterUsage);
        }
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public String getName() {
        return name;
    }

    public static int getCount() {
        return count;
    }

    public BillDirectory getBillHistory() {
        return billHistory;
    }

    public void setBillHistory(BillDirectory billHistory) {
        this.billHistory = billHistory;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isGroupOrNot() {
        return groupOrNot;
    }

    public void setGroupOrNot(boolean groupOrNot) {
        this.groupOrNot = groupOrNot;
    }

    public ArrayList<CustomerWaterUsage> getWaterUsageList() {
        return waterUsageList;
    }

    public void setWaterUsageList(ArrayList<CustomerWaterUsage> waterUsageList) {
        this.waterUsageList = waterUsageList;
    }

    public String getWorkcompany() {
        return workCompany;
    }

    public void setWorkcompany(String workCompany) {
        this.workCompany = workCompany;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getSsnnumber() {
        return ssnnumber;
    }

    public void setSsnnumber(String ssnnumber) {
        this.ssnnumber = ssnnumber;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
