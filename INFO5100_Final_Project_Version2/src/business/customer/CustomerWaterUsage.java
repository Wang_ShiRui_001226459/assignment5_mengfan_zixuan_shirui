/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business.customer;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public class CustomerWaterUsage {

    private int waterAmount=0;//cubic feet
    private int sewerAmont=0;
    private String startDate;
    
    public CustomerWaterUsage(){
        Date date = new Date();
        long times = date.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.startDate = formatter.format(date);
    }

    public int getWaterAmount() {
        return waterAmount;
    }

    public void setWaterAmount(int waterAmount) {
        this.waterAmount = waterAmount;
    }

    public int getSewerAmont() {
        return sewerAmont;
    }

    public void setSewerAmont(int sewerAmont) {
        this.sewerAmont = sewerAmont;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    
    
}
