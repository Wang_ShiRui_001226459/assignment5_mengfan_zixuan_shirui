/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business.bill;

import business.customer.CustomerWaterUsage;
import business.useraccount.UserAccount;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public class MonthlyBill extends Bill{
    
    private UserAccount receiver;
    private CustomerWaterUsage customerWaterUsage;
    private int sewerCost=0;//  67/1000cubics
    private int waterCost=0;//  49/1000

    public MonthlyBill() {
        super();
        this.setBilltype(2);
        this.customerWaterUsage=new CustomerWaterUsage();
    }

    public int getSewerCost() {
        return sewerCost;
    }

    public void setSewerCost(int sewerCost) {
        this.sewerCost = sewerCost;
    }

    public int getWaterCost() {
        return waterCost;
    }

    public void setWaterCost(int waterCost) {
        this.waterCost = waterCost;
    }

    public int getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(int billAmount) {
        this.billAmount = billAmount;
    }

    public int getBilltype() {
        return billtype;
    }

    public void setBilltype(int billtype) {
        this.billtype = billtype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(UserAccount receiver) {
        this.receiver = receiver;
    }

    public CustomerWaterUsage getCustomerWaterUsage() {
        return customerWaterUsage;
    }

    public void setCustomerWaterUsage(CustomerWaterUsage customerWaterUsage) {
        this.customerWaterUsage = customerWaterUsage;
    }
    
}
