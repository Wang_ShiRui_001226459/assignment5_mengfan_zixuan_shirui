/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business.bill;

import business.item.ItemDirectory;
import business.workqueue.CallForMaintainceWorkRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public class MaintainBill extends Bill{
    
    private CallForMaintainceWorkRequest callForMaintainceWorkRequest=new CallForMaintainceWorkRequest();
   

    public MaintainBill(){
        super();
        this.setBilltype(1);
        
    }

    public int getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(int billAmount) {
        this.billAmount = billAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getBilltype() {
        return billtype;
    }

    public void setBilltype(int billtype) {
        this.billtype = billtype;
    }

    public CallForMaintainceWorkRequest getCallForMaintainceWorkRequest() {
        return callForMaintainceWorkRequest;
    }

    public void setCallForMaintainceWorkRequest(CallForMaintainceWorkRequest callForMaintainceWorkRequest) {
        this.callForMaintainceWorkRequest = callForMaintainceWorkRequest;
    }
    
    
}
