/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.bill;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Accelarator
 */
public abstract class Bill {
    protected int billAmount = 0;
    protected int billtype = 0;//1 for maintain, 2 for monthly
    protected String status;
    private String dateString;

    public Bill() {
        this.status = "Unpaid";

        Date date = new Date();
        long times = date.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.dateString = formatter.format(date);
        System.out.println(dateString);
    }
    
    public int getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(int billAmount) {
        this.billAmount = billAmount;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public String getDateString() {
        return dateString;
    }

    public int getBilltype() {
        return billtype;
    }

    public void setBilltype(int billtype) {
        this.billtype = billtype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public String toString() {
        if (this.billtype == 1) {
            return "Maintain Bill";
        } else {
            return "Monthly Bill";
        }
    }
    
}
