/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.bill;

import java.util.ArrayList;

/**
 *
 * @author Accelarator
 */
public class BillDirectory {

    private ArrayList<Bill> billList = new ArrayList<>();

    public void addBill(Bill bill){
        this.billList.add(bill);
    }
    
    public void removeBill(Bill bill){
        this.billList.remove(bill);
    }

    public ArrayList<Bill> getBillList() {
        return billList;
    }

    public void setBillList(ArrayList<Bill> billList) {
        this.billList = billList;
    }
    
}
