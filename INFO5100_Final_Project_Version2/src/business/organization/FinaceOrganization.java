/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.inventory.Inventory;
import business.inventory.InventoryDirectory;
import business.organization.*;
import business.role.AccountantRole;
//import business.role.BillAccountantRole;
import business.role.ConstructorRole;
import business.role.PurchaseRole;
import business.role.Role;
import java.util.ArrayList;

/**
 *
 * @author xumengfan
 */
public class FinaceOrganization extends Organization {

    private InventoryDirectory inventoryDirectory = new InventoryDirectory();
    private int enterpriseDeposit;

    public FinaceOrganization() {
        super(Type.Finance.getValue());
        enterpriseDeposit = 0;
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new AccountantRole());
        //roles.add(new BillAccountantRole());
        roles.add(new PurchaseRole());
        roles.add(new ConstructorRole());
        return roles;
    }

    public InventoryDirectory getInventoryDirectory() {
        return inventoryDirectory;
    }

    public void setInventoryDirectory(InventoryDirectory inventoryDirectory) {
        this.inventoryDirectory = inventoryDirectory;
    }

    public int getEnterpriseDeposit() {
        return enterpriseDeposit;
    }

    public void setEnterpriseDeposit(int enterpriseDeposit) {
        this.enterpriseDeposit = enterpriseDeposit;
    }

}
