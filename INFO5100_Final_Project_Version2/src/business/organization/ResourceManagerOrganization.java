/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.organization.*;
import business.role.ConstructorRole;
import business.role.DevelopmentRole;
import business.role.Role;
import java.util.ArrayList;

/**
 *
 * @author xumengfan
 */
public class ResourceManagerOrganization extends Organization{

    public ResourceManagerOrganization() {
        super(Type.ResourceManager.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
       ArrayList<Role> roles = new ArrayList<>();
       roles.add(new DevelopmentRole());
       
        return roles;
    }
    
}
