/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.organization.Organization.Type;
import business.role.CustomerRole;
import business.role.Role;
import business.role.Role.RoleType;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public class CustomerOrganization extends Organization {

    public CustomerOrganization() {
        super(Type.Customer.getValue());
        for (int i = 0; i < 1000; i++) {
            //this.getCustomerDirectory().createCustomer(randIndiName());
            String name = randIndiName();

            CustomerRole ro = null;
            for (Role r : this.getSupportedRole()) {
                if (r instanceof CustomerRole) {
                    ro = (CustomerRole) r;
                }
            }

            this.getUserAccountDirectory().createUserAccount(name, "123", this.getCustomerDirectory().createCustomer(name), ro);
        }
    }

    public static String randIndiName() {
        Random rand = new Random();

        String[] array = {"Amy", "Arthur", "Carrie", "Debbie", "Emma", "Kim", "Youko", "Ling", "Hang", "Nan", "Linden ", "Nancy", "John", "Mitsouko", "Randy", "Ken", "Luke", "Kiko", "Peter", "Natalia", "Fiona", "Alex", "Ryan", "Bob", "Teresa", "Ryan", "Toothless", "Midnight", "Frogface", "Tim", "Peggy", "Charlie", "Allen", "Micheal", "Cameron", "Don", "Anita", "Jude", "Billy", "Sam", "Mickey", "Ian"};
        String name = array[rand.nextInt(array.length)];
        return name;
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new CustomerRole());
        return roles;
    }
}
