/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.organization.*;
import business.organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author xumengfan
 */
public class OrganizationDirectory {

    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }

    public Organization createOrganization(Type type) {
        Organization organization = null;
        /*        if (type.getValue().equals(Type.AccountManager.getValue())) {
            organization = new AccountManagerOrganization();
            organizationList.add(organization);
        } else*/ if (type.getValue().equals(Type.CustomerService.getValue())) {
            organization = new CustomerServiceOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.Finance.getValue())) {
            organization = new FinaceOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.ResourceManager.getValue())) {
            organization = new ResourceManagerOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.Customer.getValue())) {
            organization = new CustomerOrganization();
            organizationList.add(organization);
        }
        return organization;
    }

}
