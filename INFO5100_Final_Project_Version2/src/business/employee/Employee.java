/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business.employee;

/**
 * 
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public class Employee {

    private String name;
    private int id;
    private static int count = 1;
//
//    private boolean restOrNot=true;
    
    private int restOrNot=0;
    
    public Employee() {
        id = count;
        count++;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public String getName() {
        return name;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Employee.count = count;
    }

    public int getRestOrNot() {
        return restOrNot;
    }

    public void setRestOrNot(int restOrNot) {
        this.restOrNot = restOrNot;
    }

//    public boolean isRestOrNot() {
//        return restOrNot;
//    }
//
//    public void setRestOrNot(boolean restOrNot) {
//        this.restOrNot = restOrNot;
//    }

    @Override
    public String toString() {
        return name;
    }
}
