/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.employee.accountant;

import business.employee.Employee;

/**
 *
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public class Accountant extends Employee {

    private int accountantType; //1 bill, 2 maintain, 3 decision
    private int companyDeposit;
    
    public Accountant() {
        super();
    }

    public int getAccountantType() {
        return accountantType;
    }

    public void setAccountantType(int accountType) {
        this.accountantType = accountType;
    }

    public int getCompanyDeposit() {
        return companyDeposit;
    }

    public void setCompanyDeposit(int companyDeposit) {
        this.companyDeposit = companyDeposit;
    }

}
