/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.enterprise;

import business.resource.WaterResourceDirectory;
import business.role.Role;
import java.util.ArrayList;

/**
 *
 * @author Accelarator
 */
public class WaterConservationEnterprise extends Enterprise {

    private WaterResourceDirectory waterResourceDirectory=new WaterResourceDirectory();

    public WaterConservationEnterprise(String name) {
        super(name, EnterpriseType.WaterConservationEnterprise);
        this.getOrganizationDirectory().createOrganization(Type.CustomerService);
        this.getOrganizationDirectory().createOrganization(Type.Customer);
        this.getOrganizationDirectory().createOrganization(Type.Finance);
        this.getOrganizationDirectory().createOrganization(Type.ResourceManager);
    }

    public WaterResourceDirectory getWaterResourceDirectory() {
        return waterResourceDirectory;
    }

    public void setWaterResourceDirectory(WaterResourceDirectory waterResourceDirectory) {
        this.waterResourceDirectory = waterResourceDirectory;
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }
}
