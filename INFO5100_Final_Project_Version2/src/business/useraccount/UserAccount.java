/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business.useraccount;

import business.customer.Customer;
import business.employee.Employee;
import business.role.Role;
import business.workqueue.WorkQueue;

/**
 * 
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public class UserAccount {
    
    private Role role;
    private String username;
    private String password;
    private Employee employee;
    private WorkQueue workQueue;
    
    private Customer customer;
   
    public UserAccount() {
        workQueue = new WorkQueue();
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Employee getEmployee() {
        return employee;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return username;
    }
    
    
}
