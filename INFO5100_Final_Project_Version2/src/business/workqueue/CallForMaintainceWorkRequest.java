/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business.workqueue;

import business.item.Item;
import business.item.ItemDirectory;
import business.useraccount.UserAccount;
import java.util.ArrayList;

/**
 * 
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public class CallForMaintainceWorkRequest extends WorkRequest{

    private boolean emergencyOrNot;
    private UserAccount userAccount;
    private ItemDirectory newItemDirectory = new ItemDirectory();//new object
    private String description;
    private int stage;
    private int wholeprice;
    private int purchaseprice;
   
    
    public boolean isEmergencyOrNot() {
        return emergencyOrNot;
    }

    public void setEmergencyOrNot(boolean emergencyOrNot) {
        this.emergencyOrNot = emergencyOrNot;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ItemDirectory getNewItemDirectory() {
        return newItemDirectory;
    }

    public void setNewItemDirectory(ItemDirectory newItemDirectory) {
        this.newItemDirectory = newItemDirectory;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public int getWholeprice() {
        return wholeprice;
    }

    public void setWholeprice(int wholeprice) {
        this.wholeprice = wholeprice;
    }

    public int getPurchaseprice() {
        return purchaseprice;
    }

    public void setPurchaseprice(int purchaseprice) {
        this.purchaseprice = purchaseprice;
    }
    
    @Override
    public String toString(){
        return description;
    }
    
}
