/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue;

import business.resource.WaterResource;
import business.resource.WaterResourceDirectory;
import business.useraccount.UserAccount;

/**
 *
 * @author Accelarator
 */
public class CallForDevelopmentWorkRequest extends WorkRequest {
     private UserAccount userAccount; 
     private int stage;
     private WaterResource resource=new WaterResource();

     private String description;
//     private int level;
//     private int cost;
     private int progress=0;
     private String lastupdate;
     
     
    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public WaterResource getResource() {
        return resource;
    }

    public void setResource(WaterResource resource) {
        this.resource = resource;
    }

////    public int getEstimateCost() {
//        return estimateCost;
//    }
//
//    public void setEstimateCost(int estimateCost) {
//        this.estimateCost = estimateCost;
//    }
//
//    public int getAdvicedDevelopmentLevel() {
//        return advicedDevelopmentLevel;
//    }
//
//    public void setAdvicedDevelopmentLevel(int advicedDevelopmentLevel) {
//        this.advicedDevelopmentLevel = advicedDevelopmentLevel;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }

//    public int getLevel() {
//        return level;
//    }
//
//    public void setLevel(int level) {
//        this.level = level;
//    }
//
//    public int getCost() {
//        return cost;
//    }
//
//    public void setCost(int cost) {
//        this.cost = cost;
//    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public String getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(String lastupdate) {
        this.lastupdate = lastupdate;
    }

     @Override
    public String toString(){
        return this.resource.getWaterResourceName();
    }
     
     
}
