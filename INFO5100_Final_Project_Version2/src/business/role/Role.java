/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business.role;

import business.EcoSystem;
import business.enterprise.Enterprise;
import business.organization.Organization;
import business.useraccount.UserAccount;
import javax.swing.JPanel;

/**
 * 
 * @author Shirui.Wang<wangshirui19940202@hotmail.com>
 */
public abstract class Role {

    public enum RoleType{
        
        AccountManager("AccountManager"),
        /*BillAccountant("BillAccountant"),*/Accoutant("Accountant"),
        Customer("Customer"),
        Purchase("Purchase"),Constructor("Constructor"),
        Operator("Operator"),
        Development("Development");
        
        private String value;
        
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            UserAccount account, 
            Organization organization, 
            Enterprise enterprise, 
            EcoSystem business);

    @Override
    public String toString() {
        return this.getClass().getName();
    }
    
}
