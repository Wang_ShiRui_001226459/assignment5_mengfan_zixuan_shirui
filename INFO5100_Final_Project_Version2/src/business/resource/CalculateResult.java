/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.resource;

/**
 *
 * @author Accelarator
 */
public class CalculateResult {
    private int cost;
    private int level;

    public CalculateResult(int cost, int level) {
        this.cost = cost;
        this.level = level;
    }
    
   public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
    
    
    
    
}
