/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.resource;

/**
 *
 * @author Accelarator
 */
public class Calculation {

    private WaterResource waterresource;
    private int waterQuantity; //   1--10
    private boolean drinkingOrNot;
    private int storageCapacity;//200------100000
    private int irrgationArea;// 5---4000
    private int flowVolume;//20---2500
    private int annualFlow;//20---10000
    private boolean dryseasonOrNot;

    private int advicedDevelopmentLevel;
    private int estimateCost;

    public Calculation(WaterResource waterresource) {
        this.waterresource = waterresource;
        this.waterQuantity = waterresource.getWaterQuantity();
        this.drinkingOrNot = waterresource.isDrinkingOrNot();
        this.storageCapacity = waterresource.getStorageCapacity();
        this.irrgationArea = waterresource.getIrrigationArea();
        this.flowVolume = waterresource.getFlowVolume();
        this.annualFlow = waterresource.getAnnualFlow();
        this.dryseasonOrNot = waterresource.isDryseasonOrNot();
        //
        this.advicedDevelopmentLevel = advicedDevelopmentLevel;
        this.estimateCost = estimateCost;
    }

    public CalculateResult CalculateCost() {

        CalculateResult calculateResult;
        int result1 = 0;
        int result2 = 0;
        if (drinkingOrNot == true) {
            int num1 = waterQuantity;
            if (1 <= num1 && num1 <= 3) {
                result1 = -1;
                result2 = 0;
            } else if (4 <= num1 && num1 <= 6) {
                result1 = 2;
                result2 = 1;
            } else if (7 <= num1 && num1 <= 10) {
                result1 = 5;
                result2 = 1;
            }
            int num2 = storageCapacity;
            if (200 <= num2 && num2 < 500) {
                result1 = result1 + 3;
                result2 = result2 + 1;
            } else if (500 <= num2 && num2 <= 1000) {
                result1 = result1 + 7;
                result2 = result2 + 1;
            } else if (1001 <= num2 && num2 <= 3000) {
                result1 = result1 + 11;
                result2 = result2 + 1;
            } else if (3001 <= num2 && num2 <= 5000) {
                result1 = result1 + 17;
                result2 = result2 + 1;
            } else if (5001 <= num2 && num2 <= 7500) {
                result1 = result1 + 20;
                result2 = result2 + 2;
            } else if (7501 <= num2 && num2 <= 10000) {
                result1 = result1 + 23;
                result2 = result2 + 2;
            }
            int num3 = irrgationArea;
            if (5 <= num3 && num3 < 100) {
                result1 = result1 + 1;
                result2 = result2 + 1;
            } else if (100 <= num3 && num3 <= 500) {
                result1 = result1 + 3;
                result2 = result2 + 1;
            } else if (501 <= num3 && num3 <= 1000) {
                result1 = result1 + 6;
                result2 = result2 + 2;
            } else if (1001 <= num3 && num3 <= 2000) {
                result1 = result1 + 10;
                result2 = result2 + 2;
            } else if (2001 <= num3 && num3 <= 3000) {
                result1 = result1 + 12;
                result2 = result2 + 3;
            } else if (3001 <= num3 && num3 <= 4000) {
                result1 = result1 + 15;
                result2 = result2 + 3;
            }
            int num4 = flowVolume;
            if (20 <= num4 && num4 < 100) {
                result1 = result1 + 0;
                result2 = result2 + 1;
            } else if (100 <= num4 && num4 <= 300) {
                result1 = result1 + 1;
                result2 = result2 + 1;
            } else if (301 <= num4 && num4 <= 500) {
                result1 = result1 + 2;
                result2 = result2 + 3;
            } else if (501 <= num4 && num4 <= 1000) {
                result1 = result1 + 3;
                result2 = result2 + 3;
            } else if (1001 <= num4 && num4 <= 1500) {
                result1 = result1 + 4;
                result2 = result2 + 3;
            } else if (1501 <= num4 && num4 <= 2500) {
                result1 = result1 + 5;
                result2 = result2 + 4;
            }
            int num5 = annualFlow;
            if (20 <= num5 && num5 < 1000) {
                result1 = result1 + 1;
                result2 = result2;
            } else if (1000 <= num5 && num5 <= 3000) {
                result1 = result1 + 2;
                result2 = result2;
            } else if (3001 <= num5 && num5 <= 5000) {
                result1 = result1 + 5;
                result2 = result2 + 1;
            } else if (5001 <= num5 && num5 <= 6000) {
                result1 = result1 + 5;
                result2 = result2 + 1;
            } else if (6001 <= num5 && num5 <= 7500) {
                result1 = result1 + 6;
                result2 = result2 + 2;
            } else if (7501 <= num5 && num5 <= 10000) {
                result1 = result1 + 9;
                result2 = result2 + 3;
            }
            if (dryseasonOrNot == true) {
                result1 = result1 + 4;
                result2 = result2 + 2;
            }
        }
        ///another sutuation
        if (drinkingOrNot == false) {
            int num1 = waterQuantity;
            if (1 <= num1 && num1 <= 3) {
                result1 = -2;
                result2 = 1;

            } else if (4 <= num1 && num1 <= 6) {
                result1 = 0;
                result2 = 1;

            } else if (7 <= num1 && num1 <= 10) {
                result1 = 3;
                result2 = 1;
            }
            int num2 = storageCapacity;
            if (200 <= num2 && num2 < 500) {
                result1 = result1 + 3;
                result2 = result2 + 0;
            } else if (500 <= num2 && num2 <= 1000) {
                result1 = result1 + 7;
                result2 = result2 + 0;
            } else if (1001 <= num2 && num2 <= 3000) {
                result1 = result1 + 11;
                result2 = result2 + 0;
            } else if (3001 <= num2 && num2 <= 5000) {
                result1 = result1 + 17;
                result2 = result2 + 1;
            } else if (5001 <= num2 && num2 <= 7500) {
                result1 = result1 + 20;
                result2 = result2 + 1;
            } else if (7501 <= num2 && num2 <= 1000) {
                result1 = result1 + 23;
                result2 = result2 + 1;
            }
            int num3 = irrgationArea;
            if (5 <= num3 && num3 < 100) {
                result1 = result1 + 0;
                result2 = result2 + 1;
            } else if (100 <= num3 && num3 <= 500) {
                result1 = result1 + 2;
                result2 = result2 + 1;
            } else if (501 <= num3 && num3 <= 1000) {
                result1 = result1 + 4;
                result2 = result2 + 1;
            } else if (1001 <= num3 && num3 <= 2000) {
                result1 = result1 + 5;
                result2 = result2 + 2;
            } else if (2001 <= num3 && num3 <= 3000) {
                result1 = result1 + 6;
                result2 = result2 + 3;
            } else if (3001 <= num3 && num3 <= 4000) {
                result1 = result1 + 8;
                result2 = result2 + 3;
            }
            int num4 = flowVolume;
            if (20 <= num4 && num4 < 100) {
                result1 = result1 + 0;
                result2 = result2;
            } else if (100 <= num4 && num4 <= 300) {
                result1 = result1 + 1;
                result2 = result2 + 1;
            } else if (301 <= num4 && num4 <= 500) {
                result1 = result1 + 2;
                result2 = result2 + 1;
            } else if (501 <= num4 && num4 <= 1000) {
                result1 = result1 + 3;
                result2 = result2 + 1;
            } else if (1001 <= num4 && num4 <= 1500) {
                result1 = result1 + 4;
                result2 = result2 + 1;
            } else if (1501 <= num4 && num4 <= 2500) {
                result1 = result1 + 5;
                result2 = result2 + 2;
            }
            int num5 = annualFlow;
            if (20 <= num5 && num5 < 1000) {
                result1 = result1 + 1;
                result2 = result2;
            } else if (1000 <= num5 && num5 <= 3000) {
                result1 = result1 + 2;
                result2 = result2 + 1;
            } else if (3001 <= num5 && num5 <= 5000) {
                result1 = result1 + 5;
                result2 = result2 + 1;
            } else if (5001 <= num5 && num5 <= 6000) {
                result1 = result1 + 5;
                result2 = result2 + 1;
            } else if (6001 <= num5 && num5 <= 7500) {
                result1 = result1 + 6;
                result2 = result2 + 2;
            } else if (7501 <= num5 && num5 <= 10000) {
                result1 = result1 + 9;
                result2 = result2 + 3;
            }
            if (dryseasonOrNot == true) {
                result1 = result1 + 4;
                result2 = result2;
            }
        }
        calculateResult = new CalculateResult(result1, result2);
        return calculateResult;

    }
}
