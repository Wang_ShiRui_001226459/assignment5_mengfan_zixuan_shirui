/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.resource;

import static business.RandomGenerator.randCommunityName;
import static business.RandomGenerator.randInt;
import java.util.ArrayList;
/**
 *
 * @author Accelarator
 */
public class WaterResourceDirectory {

    private ArrayList<WaterResource> waterResourceDirectory;

    public WaterResourceDirectory() {
        waterResourceDirectory = new ArrayList<WaterResource>();
        newWaterResourcesIni();
    }

    public void newWaterResourcesIni(){
        
        for (int i = 0; i < randInt(4, 20); i++) {
            String locaion = randCommunityName();
            WaterResource wr = new WaterResource(locaion, locaion, "", randInt(1, 10), true, randInt(200, 10000), randInt(5, 4000), randInt(20, 2500), randInt(20, 10000), false);
            this.waterResourceDirectory.add(wr);
        }
    }
   

    public ArrayList<WaterResource> getWaterResourceDirectory() {
        return waterResourceDirectory;
    }

    public void setWaterResourceDirectory(ArrayList<WaterResource> waterResourceDirectory) {
        this.waterResourceDirectory = waterResourceDirectory;
    }

    public WaterResource createWaterResource(String waterResourceName, String location, String description, int waterQuantity, boolean drinkingOrNot, int storageCapacity, int irrigationArea, int flowVolume, int annualFlow,  boolean dryseasonOrNot) {
        WaterResource resource = new WaterResource(waterResourceName, location, description, waterQuantity, drinkingOrNot, storageCapacity, irrigationArea, flowVolume, annualFlow, dryseasonOrNot);
        waterResourceDirectory.add(resource);
        return resource;
    }
     public WaterResource removeWaterResource(String waterResourceName, String location, String description, int waterQuantity, boolean drinkingOrNot, int storageCapacity, int irrigationArea, int flowVolume, int annualFlow,  boolean dryseasonOrNot) {
        WaterResource resource = new WaterResource(waterResourceName, location, description, waterQuantity, drinkingOrNot, storageCapacity, irrigationArea, flowVolume, annualFlow, dryseasonOrNot);
        waterResourceDirectory.remove(resource);
        return resource;
    }
}

