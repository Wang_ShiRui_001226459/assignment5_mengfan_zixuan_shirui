/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.resource;

/**
 *
 * @author Accelarator
 */
public class WaterResource {

    private String waterResourceName;
    private String location;
    private String description;
    //evaluation
    private int waterQuantity;
    private boolean drinkingOrNot;
    private int storageCapacity;
    private int irrigationArea;
    private int flowVolume;
    private int annualFlow;
    private boolean dryseasonOrNot;

    private int advicedDevelopmentLevel;
    private int estimateCost;

    public WaterResource() {

    }//  !!!

    public WaterResource(String waterResourceName, String location, String description, int waterQuantity, boolean drinkingOrNot, int storageCapacity, int irrigationArea, int flowVolume, int annualFlow,  boolean dryseasonOrNot){
        this.waterResourceName = waterResourceName;
        this.location = location;
        this.description = description;
        this.waterQuantity = waterQuantity;
        this.drinkingOrNot = drinkingOrNot;
        this.storageCapacity = storageCapacity;
        this.irrigationArea = irrigationArea;
        this.flowVolume = flowVolume;
        this.annualFlow = annualFlow;
        this.dryseasonOrNot = dryseasonOrNot;

        this.advicedDevelopmentLevel = advicedDevelopmentLevel;
        this.estimateCost = estimateCost;

    }

    public String getWaterResourceName() {
        return waterResourceName;
    }

    public void setWaterResourceName(String waterResourceName) {
        this.waterResourceName = waterResourceName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWaterQuantity() {
        return waterQuantity;
    }

    public void setWaterQuantity(int waterQuantity) {
        this.waterQuantity = waterQuantity;
    }

    public boolean isDrinkingOrNot() {
        return drinkingOrNot;
    }

    public void setDrinkingOrNot(boolean drinkingOrNot) {
        this.drinkingOrNot = drinkingOrNot;
    }

    public int getStorageCapacity() {
        return storageCapacity;
    }

    public void setStorageCapacity(int storageCapacity) {
        this.storageCapacity = storageCapacity;
    }

    public int getIrrigationArea() {
        return irrigationArea;
    }

    public void setIrrigationArea(int irrigationArea) {
        this.irrigationArea = irrigationArea;
    }

    public int getFlowVolume() {
        return flowVolume;
    }

    public void setFlowVolume(int flowVolume) {
        this.flowVolume = flowVolume;
    }

    public int getAnnualFlow() {
        return annualFlow;
    }

    public void setAnnualFlow(int annualFlow) {
        this.annualFlow = annualFlow;
    }

    public boolean isDryseasonOrNot() {
        return dryseasonOrNot;
    }

    public void setDryseasonOrNot(boolean dryseasonOrNot) {
        this.dryseasonOrNot = dryseasonOrNot;
    }

    public int getAdvicedDevelopmentLevel() {
        return advicedDevelopmentLevel;
    }

    public void setAdvicedDevelopmentLevel(int advicedDevelopmentLevel) {
        this.advicedDevelopmentLevel = advicedDevelopmentLevel;
    }

    public int getEstimateCost() {
        return estimateCost;
    }

    public void setEstimateCost(int estimateCost) {
        this.estimateCost = estimateCost;
    }

  

  @Override
    public String toString() {
        return waterResourceName;
    }
   
    
    
}